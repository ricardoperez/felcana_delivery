defmodule FelcanaDelivery.Fulfiller do
  alias FelcanaDelivery.Geometry.Point

  @type t :: %__MODULE__{point: Point.t(), name: String.t()}
  defstruct [:point, :name]
end
