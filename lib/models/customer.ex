defmodule FelcanaDelivery.Customer do
  alias FelcanaDelivery.Geometry.Point
  alias FelcanaDelivery.Fulfiller

  @type t :: %__MODULE__{point: Point.t(), name: String.t(), fulfiller: Fulfiller.t()}
  defstruct [:point, :name, :fulfiller]
end
