defmodule FelcanaDelivery.Geometry.Point do
  @type t :: %__MODULE__{coordinates: {number, number}, type: String.t()}
  defstruct [:coordinates, :type]
end
