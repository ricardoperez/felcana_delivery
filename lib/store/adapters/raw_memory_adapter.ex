defmodule FelcanaDelivery.Store.RawMemoryAdapter do
  use Agent
  alias FelcanaDelivery.Store.Adapter
  alias FelcanaDelivery.Math.Distance
  alias FelcanaDelivery.{Customer, Fulfiller}

  @behaviour Adapter
  @num_of_cores :erlang.system_info(:logical_processors)

  def start_link(_) do
    Agent.start_link(fn -> %{customers: %{}, fulfillers: %{}} end, name: __MODULE__)
  end

  @impl Adapter
  def save(data = %Customer{}, key) do
    do_save(data, :customers, key)
  end

  def save(data = %Fulfiller{}, key) do
    do_save(data, :fulfillers, key)
  end

  @impl Adapter
  def fetch(%Customer{}, key) do
    do_fetch(:customers, key)
  end

  def fetch(%Fulfiller{}, key) do
    do_fetch(:fulfillers, key)
  end

  @impl Adapter
  def all(%Customer{}) do
    Agent.get(__MODULE__, fn state -> state[:customers] end)
  end

  def all(%Fulfiller{}) do
    Agent.get(__MODULE__, fn state -> state[:fulfillers] end)
  end

  @impl Adapter
  def find_nearest_fulfiller_for_all_customers() do
    state =
      Agent.get(__MODULE__, fn state ->
        state
      end)

    customers = state[:customers]
    fulfillers = state[:fulfillers]

    customers
    |> chunk_data(fn subset -> map_customers(subset, fulfillers) end)
    |> List.flatten()
  end

  defp find_nearest_fulfiller(customer, fulfillers) do
    fulfillers
    |> chunk_data(fn subset -> reduce_fulfillers(subset, customer) end)
    |> Enum.sort()
    |> List.first()
    |> Access.get(:fulfiller)
  end

  defp map_customers(customers, fillers) do
    Enum.map(customers, fn {_k, customer} ->
      fulfiller = find_nearest_fulfiller(customer, fillers)
      %{customer | fulfiller: fulfiller}
    end)
  end

  defp reduce_fulfillers(fillers_subset, customer) do
    Enum.reduce(fillers_subset, %{}, fn {_k, item}, acc ->
      distance = Distance.calculate(customer.point, item.point)

      if is_distance_smaller?(acc[:distance], distance) do
        %{fulfiller: item, distance: distance}
      else
        acc
      end
    end)
  end

  defp chunk_data(data, fun) when map_size(data) > 0 do
    data
    |> Enum.chunk_every(chunk_size(data))
    |> Enum.map(&fun.(&1))
  end

  defp chunk_data(_data, _fn), do: []

  defp is_distance_smaller?(nil, _distance), do: true

  defp is_distance_smaller?(actual_distance, distance) do
    distance < actual_distance
  end

  defp chunk_size(countable) when is_map(countable) and @num_of_cores > 2 do
    countable
    |> Enum.count()
    |> chunk_size()
  end

  defp chunk_size(c) when is_map(c), do: 2

  defp chunk_size(count) when count > @num_of_cores do
    count
    |> Kernel./(@num_of_cores)
    |> Kernel.trunc()
  end

  defp chunk_size(count), do: count

  defp do_save(data, table, key) do
    Agent.update(__MODULE__, fn state ->
      put_in(state, [table, key], data)
    end)
  end

  def do_fetch(table, key) do
    Agent.get(__MODULE__, fn state -> state[table][key] end)
  end
end
