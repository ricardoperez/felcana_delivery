defmodule FelcanaDelivery.Store do
  @moduledoc ~S"""
  Module to cache/fetch generic data.
  """

  @config Application.fetch_env!(:felcana_delivery, FelcanaDelivery.Store)
  @adapter Keyword.fetch!(@config, :adapter)

  defdelegate save(data, key), to: @adapter
  defdelegate fetch(table, key), to: @adapter
  defdelegate all(data), to: @adapter
  defdelegate find_nearest_fulfiller_for_all_customers(), to: @adapter
end
