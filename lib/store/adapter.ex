defmodule FelcanaDelivery.Store.Adapter do
  alias FelcanaDelivery.{Customer, Fulfiller}

  @moduledoc ~S"""
  Behaviour for Store adapters
  """

  @doc ~S"""
  Save data to a table.

  Returns `:ok`

  ## Examples
      iex> save(%Customer{name: "customer 1"}, "customer1")
      :ok
  """
  @callback save(data :: Customer.t() | Fulfiller.t(), key :: any) :: :ok

  @doc ~S"""
  Fetch stored data.

  Returns stored data.

  ## Examples
      iex> fetch(%Fulfiller{}. "storage1")
      %Fullfiller{name: "storage 1"}
  """
  @callback fetch(model :: Customer.t() | Fulfiller.t(), key :: any) ::
              Customer.t() | Fulfiller.t() | nil

  @doc ~S"""
  Fetch all data from the given type.

  Returns a list with the given type.

  ## Examples
      iex> all(%Fulfiller{})
      [%Fullfiller{name: "storage 1"}, ...]
  """
  @callback all(model :: Customer.t() | Fulfiller.t()) :: [Customer.t() | Fulfiller.t() | nil]

  @doc ~S"""
  Find the closesest fullfiler for all customers.
  """
  @callback find_nearest_fulfiller_for_all_customers() :: [Customer.t()]
end
