defmodule FelcanaDelivery do
  @moduledoc """
  Documentation for FelcanaDelivery.
  """
  alias FelcanaDelivery.Store
  alias FelcanaDelivery.{Fulfiller, Customer}
  alias FelcanaDelivery.Geometry.Point

  @doc """
  Add fullfiler struct to memory.
  """
  @spec add_fulfiller(Fulfiller.t()) :: :ok | {:error, atom}
  def add_fulfiller(fulfiller = %Fulfiller{name: name}) do
    save(fulfiller, name)
  end

  @doc """
  Add customer struct to memory.
  """
  @spec add_customer(Customer.t()) :: :ok | {:error, atom}
  def add_customer(customer = %Customer{name: name}) do
    save(customer, name)
  end

  @doc """
  Create customer struct and add to memory.
  """
  def create_customer(name, coord_x, coord_y) do
    create(%Customer{}, name, coord_x, coord_y)
  end

  @doc """
  Create customer struct and add to memory.
  """
  def create_fulfiller(name, coord_x, coord_y) do
    create(%Fulfiller{}, name, coord_x, coord_y)
  end

  @doc """
  Get customer by the name.
  """
  def get_customer(name) do
    Store.fetch(%Customer{}, name)
  end

  @doc """
  Get fulfiller by the name.
  """
  def get_fulfiller(name) do
    Store.fetch(%Fulfiller{}, name)
  end

  @doc """
  Get all customer.
  """
  def all_customers() do
    Store.all(%Customer{})
  end

  @doc """
  Get all fulfillers.
  """
  def all_fulfiller() do
    Store.all(%Fulfiller{})
  end

  @doc """
  Set a fulfiller for all saved customers.
  """
  def find_nearest_fulfiller_for_all_customers() do
    Store.find_nearest_fulfiller_for_all_customers()
  end

  defp save(model, key) do
    Store.save(model, key)
  end

  defp create(model, name, coord_x, coord_y) do
    model
    |> Map.put(:name, name)
    |> Map.put(:point, %Point{coordinates: {coord_x, coord_y}})
    |> save(name)
  end
end
