defmodule FelcanaDelivery.Application do
  use Application

  def start(_type, _args) do
    opts = [strategy: :one_for_one, name: FelcanaDelivery.Supervisor]

    Supervisor.start_link(children(Mix.env()), opts)
  end

  def children(:test), do: []

  def children(_) do
    [FelcanaDelivery.Store.RawMemoryAdapter]
  end
end
