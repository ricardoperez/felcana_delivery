defmodule FelcanaDelivery.Math.Distance do
  @moduledoc """
  Module reponsible for Distance calculations.
  """
  alias FelcanaDelivery.Geometry.Point

  @doc """
  Calculate distance between to points.
  """

  @spec calculate(Point.t(), Point.t()) :: number
  def calculate(%Point{coordinates: {x1, y1}}, %Point{coordinates: {x2, y2}}) do
    find_hipotenuse(x1, x2, y1, y2)
  end

  defp find_hipotenuse(x1, x2, y1, y2) do
    :math.sqrt(shib(x1, x2) + shib(y1, y2))
  end

  defp shib(point1, point2) do
    :math.pow(point2 - point1, 2)
  end
end
