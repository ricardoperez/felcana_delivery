defmodule FelcanaDelivery.TestCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      alias FelcanaDelivery.{Fulfiller, Customer}
      alias FelcanaDelivery.Geometry.Point
      alias FelcanaDelivery.Store.RawMemoryAdapter

      setup do
        RawMemoryAdapter.start_link([])

        :ok
      end

      defp setup_fulfillers(fulfillers_coord, prefix \\ "store") do
        %Fulfiller{}
        |> save_many(fulfillers_coord, prefix)
      end

      defp setup_customers(customers_coord, prefix \\ "store") do
        %Customer{}
        |> save_many(customers_coord, prefix)
      end

      defp save_many(struct, coordinates, prefix) do
        coordinates
        |> Stream.with_index()
        |> Enum.each(fn {coord, ind} ->
          struct
          |> create(coord, "#{prefix}#{ind}")
          |> save()
        end)
      end

      defp generate_fulfillers(num, prefix \\ "store") do
        StreamData.tuple({StreamData.float(), StreamData.float()})
        |> Stream.with_index()
        |> Stream.map(fn {coord, ind} -> save_fulfiller(coord, "#{prefix}#{ind}") end)
        |> Enum.take(num)
      end

      defp generate_customers(num, prefix \\ "customer") do
        StreamData.tuple({StreamData.float(), StreamData.float()})
        |> Stream.with_index()
        |> Stream.map(fn {coord, ind} -> save_customer(coord, "#{prefix}#{ind}") end)
        |> Enum.take(num)
      end

      defp create_fulfiller(coord, name) do
        create(%Fulfiller{}, coord, name)
      end

      defp create_customer(coord, name) do
        create(%Customer{}, coord, name)
      end

      defp create(struct, coord, name) do
        point = %Point{coordinates: coord}
        %{struct | point: point, name: name}
      end

      def save_fulfiller(coord, name) do
        %Fulfiller{}
        |> create(coord, name)
        |> save()
      end

      def save_customer(coord, name) do
        %Customer{}
        |> create(coord, name)
        |> save()
      end

      def save(fulfiller = %Fulfiller{}) do
        FelcanaDelivery.add_fulfiller(fulfiller)
      end

      def save(customer = %Customer{}) do
        FelcanaDelivery.add_customer(customer)
      end
    end
  end
end
