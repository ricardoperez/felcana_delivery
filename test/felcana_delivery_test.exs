defmodule FelcanaDeliveryTest do
  use FelcanaDelivery.TestCase, async: false

  alias FelcanaDelivery.{Fulfiller, Customer}
  alias FelcanaDelivery.Geometry.Point

  doctest FelcanaDelivery

  describe "add_fulfiller/1" do
    test "adding raw fulfiller struct" do
      point = %Point{coordinates: {2.5, 2.1}}

      filler = %Fulfiller{point: point, name: "shop1"}

      assert FelcanaDelivery.add_fulfiller(filler)
    end
  end

  describe "add_customer/1" do
    test "adding raw customer struct" do
      point = %Point{coordinates: {0.2, -0.1}}

      customer = %Customer{point: point, name: "customer1"}

      assert FelcanaDelivery.add_customer(customer)
    end
  end

  describe "create_customer/3" do
    test "create customer with name and coordinates" do
      assert FelcanaDelivery.create_customer("customerx", 1, 2)
    end
  end

  describe "create_fulfiller/3" do
    test "create fulfiller with name and coordinates" do
      assert FelcanaDelivery.create_fulfiller("storagex", 1, 2)
    end
  end

  describe "get_customer/1" do
    test "get customer with name" do
      FelcanaDelivery.create_customer("customery", 1, 2)
      assert %Customer{} = FelcanaDelivery.get_customer("customery")
    end
  end

  describe "get_fulfiller/1" do
    test "get fulfiller with name" do
      FelcanaDelivery.create_fulfiller("storagey", 1, 2)
      assert %Fulfiller{} = FelcanaDelivery.get_fulfiller("storagey")
    end
  end

  describe "find_nearest_fulfiller_for_all_customers/0" do
    test "set a fulfiller for each customer" do
      generate_fulfillers(10)
      generate_customers(10)

      assert FelcanaDelivery.find_nearest_fulfiller_for_all_customers()
    end
  end
end
