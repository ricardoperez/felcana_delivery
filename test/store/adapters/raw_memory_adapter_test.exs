defmodule FelcanaDelivery.Store.RawMemoryAdapterTest do
  use FelcanaDelivery.TestCase, async: false
  alias FelcanaDelivery.Store.RawMemoryAdapter

  alias FelcanaDelivery.{Fulfiller, Customer}
  alias FelcanaDelivery.Geometry.Point

  describe "save/3" do
    test "save a fulffiler." do
      fulfiller = create_fulfiller({2, 4}, "fulfiller")
      assert RawMemoryAdapter.save(fulfiller, "fulffiler")
    end

    test "save a customer." do
      customer = create_customer({1, 2}, "customer")
      assert RawMemoryAdapter.save(customer, "customer")
    end
  end

  describe "fetch/2" do
    test "returns nil when nothing is set" do
      refute RawMemoryAdapter.fetch(%Customer{}, "custfiller")
    end

    test "returns cached data" do
      name = "customer1"
      save_customer({1, 2}, name)
      assert %FelcanaDelivery.Customer{name: ^name} = RawMemoryAdapter.fetch(%Customer{}, name)
    end
  end

  describe "find_nearest_store_for_cutomer/-" do
    test "returns empty list without fulfiilers or customers" do
      assert [] = RawMemoryAdapter.find_nearest_fulfiller_for_all_customers()
    end

    test "returns list with only customer when there are no fulfillers" do
      generate_customers(10)

      refute RawMemoryAdapter.find_nearest_fulfiller_for_all_customers()
             |> Enum.map(& &1.fulfiller)
             |> Enum.reduce([], fn f, _acc -> f end)
    end

    test "returns sorted list with nearest fulfiiler for each customer" do
      fulfillers_coord = [
        {2.5, 2.1},
        {0.9, 1.8},
        {-2.5, 2.1}
      ]

      customers_coord = [
        {-2.0, 1.2},
        {0.2, -0.1},
        {2.4, 2.2},
        {1.0, 2.0}
      ]

      setup_fulfillers(fulfillers_coord)
      setup_customers(customers_coord, "custom")

      result = RawMemoryAdapter.find_nearest_fulfiller_for_all_customers()

      customer1 = Enum.at(result, 0)
      customer2 = Enum.at(result, 1)
      customer3 = Enum.at(result, 2)
      customer4 = Enum.at(result, 3)

      assert customer1.name == "custom0"
      assert customer1.fulfiller.name == "store2"

      assert customer2.name == "custom1"
      assert customer2.fulfiller.name == "store1"

      assert customer3.name == "custom2"
      assert customer3.fulfiller.name == "store0"

      assert customer4.name == "custom3"
      assert customer4.fulfiller.name == "store1"
    end

    test "returns sorted list" do
      generate_num_customers = 5000
      generate_num_fillers = 2000

      IO.puts(
        "Generating #{generate_num_customers} customers and #{generate_num_fillers} fullfilers"
      )

      t1 = :erlang.timestamp()
      generate_fulfillers(generate_num_customers)
      generate_customers(generate_num_fillers)
      t2 = :erlang.timestamp()
      IO.puts("Data Generation: #{:timer.now_diff(t2, t1) / 1_000_000}s")

      t1 = :erlang.timestamp()
      assert RawMemoryAdapter.find_nearest_fulfiller_for_all_customers()
      t2 = :erlang.timestamp()
      IO.puts("function time: #{:timer.now_diff(t2, t1) / 1_000_000}s")
    end
  end
end
