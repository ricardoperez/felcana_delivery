defmodule FelcanaDelivery.Math.DistanceTest do
  use ExUnit.Case
  alias FelcanaDelivery.Geometry.Point
  alias FelcanaDelivery.Math.Distance

  describe "calculate/2" do
    test "returns right distance(15.0) for two Points with coordinates (8.0, 2.0) and (-4.0, -7.0)" do
      point1 = %Point{coordinates: {8.0, 2.0}}
      point2 = %Point{coordinates: {-4.0, -7.0}}

      assert Distance.calculate(point1, point2) == 15.0
    end

    test "returns right distance(13.0) for two Points with coordinates (-1.0, 2.0) and (4.0, 14.0)" do
      point1 = %Point{coordinates: {-1.0, 2.0}}
      point2 = %Point{coordinates: {4.0, 14.0}}

      assert Distance.calculate(point1, point2) == 13.0
    end
  end
end
