# FelcanaDelivery

## Technical Challenge:

We are developing an app that will revolutionise local, inner-city deliveries by allowing ordinary people to make the last leg of delivery to their neighbours and earning money per item they deliver – let’s call them the fulfillers.
We know the locations of our fulfillers, so we need an algorithm to match the recipients of our orders to the closest available fulfiller.

Our aim is to match each delivery from the second list to the closest fulfiller in the first list. As both lists can get large, the more efficient your matching algorithm, the better.

## Documentation

This app was developed with elixir 1.6.5(compiled with OTP 20)

### Adding a fulfiller

```elixir
iex> handler = "handler"
iex> coordinate_x = 1.0
iex> coordinate_y = 2.3
iex> FelcanaDelivery.create_fulfiller(handler, coordinate_x, coordinate_y)
```

### Adding a Customer

```elixir
iex> handler = "customer"
iex> coordinate_x = 3.1
iex> coordinate_y = -0.8
iex> FelcanaDelivery.create_customer(handler, coordinate_x, coordinate_y)
```

### Matching Customer with the nearest Fulfiller

```elixir
iex> FelcanaDelivery.find_nearest_fulfiller_for_all_customers()
[Customer<Fulfiller>,...]
```

### Fetching a single Customer or Fulfiller

```elixir
iex> FelcanaDelivery.get_customer("customer")
%Customer{name: "customer"}

iex> FelcanaDelivery.get_fulfiller("fulfiller")
%Fulfiller{name: "fulfiller"}
```

### List all customers and fulfillers

```elixir
iex> FelcanaDelivery.all_customers()
[Customer, Customer, ...]

iex> FelcanaDelivery.all_fulfillers()
[Fulffiler]
```

### Running test

```
$ mix test
```

### Check coverage

```
$ mix coveralls
```

### Check specs

```
$ mix dialyzer
```

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `felcana_delivery` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:felcana_delivery, "~> 0.1.0"}
  ]
end
```

## Question

1. How would your code and answers change if the lists were too large to fit on a single machine?

  One possibility is to use [distributed elixir task](https://elixir-lang.org/getting-started/mix-otp/distributed-tasks-and-configuration.html), as its can be scaled to many machines as available.But the best option in such case is use a relational database with support/extension for geospatial data, like PostgreSQL with PostGIS. In the case of the app, would be necessary just a small change. Will be necessary to create a new `Adapter` under  the `DeliveryFelcana.Store.Adapters` namespace.
