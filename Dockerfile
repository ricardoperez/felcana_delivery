FROM elixir:1.6.5

RUN mkdir /app
WORKDIR /app

COPY mix.exs mix.lock ./
RUN mix local.hex --force && \
    mix deps.get

COPY . .
RUN mix deps.get && \
    mix compile
